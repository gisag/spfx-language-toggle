import * as React from 'react';
import { override } from '@microsoft/decorators';
import { Log, Guid } from '@microsoft/sp-core-library';
import {
  BaseApplicationCustomizer
} from '@microsoft/sp-application-base';
import { ExtensionService, Location } from '@valo/extensibility';

import * as strings from 'LanguageToggleApplicationCustomizerStrings';

import LanguageToggle from './components/LanguageToggle';

const LOG_SOURCE: string = 'LanguageToggleApplicationCustomizer';

/** A Custom Action which can be run during execution of a Client Side Application */
export default class LanguageToggleApplicationCustomizer
  extends BaseApplicationCustomizer<void> {

  @override
  public onInit(): Promise<void> {
    Log.info(LOG_SOURCE, `Initialized ${strings.Title}`);

    const service = ExtensionService.getInstance();
    service.registerExtension({
      id: Guid.newGuid().toString(),
      location: Location.NavigationLeft,
      element: React.createElement(LanguageToggle, {
        localPagePath: '/sites/ri-de-uber-uns/SitePages/Berlin.aspx',
        globalPagePath: '/sites/ri-de',
      }),
    });

    return Promise.resolve();
  }
}
