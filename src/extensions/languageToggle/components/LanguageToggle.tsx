import * as React from 'react';

export interface ILanguageToggleProps {
  globalPagePath: string;
  localPagePath: string;
}

export interface ILanguageToggleState {
  isGlobalPage: boolean;
  isLocalPage: boolean;
}

const divStyle = {
  position: 'relative' as 'relative',
  height: '100%'
};

const imgStyle = {
  position: 'absolute' as 'absolute',
  width: '60px',
  top: '27%'
};

export default class LanguageToggle extends React.Component<ILanguageToggleProps, ILanguageToggleState> {
  public readonly state: ILanguageToggleState;

  constructor(props: ILanguageToggleProps) {
    super(props);
    this.state = {
      isGlobalPage: (window.location.pathname.indexOf(this.props.globalPagePath) > -1) ? true : false,
      isLocalPage: (window.location.pathname.indexOf(this.props.localPagePath) > -1) ? true : false,
    };
  }

  public render() {
    console.log(this.props);
    console.log(this.state);
    if (this.state.isLocalPage) {
      return (
        <div style={divStyle}>
          <a href={`https://${window.location.hostname}${this.props.globalPagePath}`}>
            <img src={require('../assets/local.png')} style={imgStyle} />
          </a>
        </div>
      );
    }

    if (this.state.isGlobalPage) {
      return (
        <div style={divStyle}>
          <a href={`https://${window.location.hostname}${this.props.localPagePath}`}>
            <img src={require('../assets/global.png')} style={imgStyle} />
          </a>
        </div>
      );
    }

    return null;
  }
}