declare interface ILanguageToggleApplicationCustomizerStrings {
  Title: string;
}

declare module 'LanguageToggleApplicationCustomizerStrings' {
  const strings: ILanguageToggleApplicationCustomizerStrings;
  export = strings;
}
